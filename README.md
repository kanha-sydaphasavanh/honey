# Honey

## Prerequisites
### Configuring upload directory
#### Create a folder and name it "*uploads*" in the public repository
```
## In two steps
$ cd public
$ mkdir uploads

## Or in one step
$ mkdir public/uploads

```

### Configuring .env file
#### Create an .env file at the base of the project
```
$ touch .env
```
#### Copy the content bellow and replace the values by yours 
```
EMAIL_SENDER= <your_email>
EMAIL_SENDER_PASSWORD= <your_password>
```


